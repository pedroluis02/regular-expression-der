# Regular Expression Derivation
Derivate a simple regular expression.

#### Regular expression example:
```
aa+cb*c+
```

#### Output example: 10 levels
```
aacc
aaacbcc
aaaacbbccc
aaaaacbbbcccc
aaaaaacbbbbccccc
aaaaaaacbbbbbcccccc
aaaaaaaacbbbbbbccccccc
aaaaaaaaacbbbbbbbcccccccc
aaaaaaaaaacbbbbbbbbccccccccc
aaaaaaaaaaacbbbbbbbbbcccccccccc
```
