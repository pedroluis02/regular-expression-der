/*
 ============================================================================
 Name        : Regular expression derivation
 Author      : Pedro Luis
 ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct NodePL {
	int position;
	char *str;
	char pattern[1];
	struct NodePL * next;
};

// globalpl
int number_op  = 0;
struct NodePL * list_pl = NULL;

int isAlpha(char c) {
	int ascii = (int)c;
	return ascii >= 97 && ascii <= 122;
}

int isValidOperator(char c) {
	return c == '+' || c == '*';
}

int parsingExpression(const char *expr, int length, int iterations) {
	int i = 0, state = 0;
	char c;
	while(i < length) {
		c = expr[i];
		if(state == 0 && isAlpha(c)) {
			state = 1;
		} else if(state == 1 && isAlpha(c)) {
			state = 1;
		} else if(state == 1 && isValidOperator(c)) {
			struct NodePL * n_pl = (struct NodePL*)malloc(sizeof(struct NodePL));
			n_pl->position = i - 1;
			n_pl->pattern[0] = expr[i-1];
			if(c == '*') {
				n_pl->str = (char *)malloc(sizeof(char) * (iterations-1));
				strcpy(n_pl->str, "");
			} else {
				n_pl->str = (char *)malloc(sizeof(char) * iterations);
				strcpy(n_pl->str, strdup(n_pl->pattern));
			}

			n_pl->next = NULL;

			if(list_pl == NULL) {
				list_pl = n_pl;
			} else {
				struct NodePL *aux = list_pl;
				while(aux->next != NULL) {
					aux = aux->next;
				}
				aux->next = n_pl;
			}

			number_op++;
		} else if(state == 2 && isAlpha(c)) {
			state = 1;
		} else {
			break;
		}
		i++;
	}

	if((state == 1 || state == 2) && i==length) {
		return -1;
	} else {
		return i;
	}
}

int main(void) {
	const char *expr = "aa+cb*c+";
	int it = 25; // iterations
	int length = strlen(expr);
	int position = parsingExpression(expr, length, it);
	if(position != -1) {
		printf("Stop in character[%d]: %c", position, expr[position]);
		return EXIT_SUCCESS;
	}

	/*struct NodePL *np = list_pl;
	while(np != NULL) {
		//printf("%d ", np->position);
		np = np->next;
	}*/

	int i, p = 0;
	char * c = (char *)malloc(sizeof(char));
	char * str_level;
	struct NodePL * aux_pl;
	for(i = 0; i < it; i++) {
		p = 0;
		str_level = strdup("");
		while(p < length) {
			c[0] = expr[p];
			if(isAlpha(expr[p])) {
				aux_pl = list_pl;
				while(aux_pl != NULL) {
					if(aux_pl->position == p) {
						break;
					}
					aux_pl = aux_pl->next;
				}
				if(aux_pl == NULL) {
					strcat(str_level, c);
				} else {
					strcat(str_level, aux_pl->str);
					strcat(aux_pl->str, aux_pl->pattern);
				}
			}
			p++;
		}
		//printf("%d: %s\n", i, str_level);
		printf("%s\n", str_level);
        free(str_level);
	}

	struct NodePL *np = list_pl;
	while(np != NULL) {
		free(np->str);
		np = np->next;
	}

	return EXIT_SUCCESS;
}
